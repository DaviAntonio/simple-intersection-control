/*
 *  Simple Intersection Controller - simulate an intersection on a Raspberry Pi
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Simple Intersection Controller.
 *
 *  Simple Intersection Controller is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#include "messages.h"

void serialise_msg(struct intersection_event e, char *buf, int *len)
{
	memset(buf, 0, MSG_LEN);
	*len = snprintf(buf, MAX_MSG_LEN,
			"M TYPE:%d PAYLOAD:%d TSEC:%ld TNSEC:%ld M",
			e.type, e.payload, e.time.tv_sec, e.time.tv_nsec);
}

struct intersection_event deserialise(char *buf)
{
	struct intersection_event e;
	int result;
	int n;

	result = sscanf(buf, "M TYPE:%d PAYLOAD:%d TSEC:%ld TNSEC:%ld M%n",
			&e.type, &e.payload, &e.time.tv_sec, &e.time.tv_nsec,
			&n);
	if (result == 4 && n > 0 && buf[n] == '\0') {
		return e;
	} else {
		e.type = INVALID;
		return e;
	}
}
