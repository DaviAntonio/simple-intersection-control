/*
 *  Simple Intersection Controller - simulate an intersection on a Raspberry Pi
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Simple Intersection Controller.
 *
 *  Simple Intersection Controller is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> // memset
#include <errno.h>
#include <error.h>
#include <arpa/inet.h> // inet_pton
#include <sys/types.h>
#include <unistd.h> // fork
#include <sys/wait.h>

#define N_ISEC_PIDS (2)

int main (int argc, char **argv)
{
	int result;
	struct sockaddr_in srv_addr;
	pid_t isec_pids[N_ISEC_PIDS];
	int status[N_ISEC_PIDS];

	if (argc != 3) {
		printf("%s server_ip server_port\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	// server's address
	memset(&srv_addr, 0, sizeof(srv_addr));
	srv_addr.sin_family = AF_INET;
	result = inet_pton(AF_INET, argv[1], &(srv_addr.sin_addr));
	if (result <= 0) {
		if (!result) {
			fprintf(stderr, "'%s' is not a valid address\n",
					argv[1]);
			exit(EXIT_FAILURE);
		} else {
			error_at_line(-1, errno, __FILE__, __LINE__,
					"inet_pton()");
		}
	}

	if (sscanf(argv[2], "%hu", &(srv_addr.sin_port)) != 1) {
		fprintf(stderr, "'%s' is an invalid port\n", argv[2]);
		exit(EXIT_FAILURE);
	}

	char *args[N_ISEC_PIDS][18] = {
		{"./intersection_controller", argv[1], argv[2],
			"12", "21", // reds
			"16", "26", // yellows
			"20", "1", // greens
			"7", "8", // pedestrians
			"14", "15", // go
			"18", "23", "24", "25", // speed
			NULL
		},
		{"./intersection_controller", argv[1], argv[2],
			"6", "11", // reds
			"5", "3", // yellows
			"0", "2", // greens
			"9", "10", // pedestrians
			"4", "17", // go
			"27", "22", "13", "19", // speed
			NULL
		}
	};

	for (int i = 0; i < N_ISEC_PIDS; i++) {
		if ((isec_pids[i] = fork()) < 0) {
			error_at_line(-1, errno, __FILE__, __LINE__,
					"fork()");
		} else if (isec_pids[i] == 0) {
			int result;
			result = execv("./intersection_controller", args[i]);
			if (result == -1) {
				error_at_line(-1, errno, __FILE__, __LINE__,
					"execv()");
			}
			exit(0);
		} else {
			printf("Child %d alive\n", isec_pids[i]);
		}
	}

	// avoid zombies
	for (int i = 0; i < N_ISEC_PIDS; i++) {
		pid_t cpid;
		cpid = waitpid(isec_pids[i], &status[i], 0);
		if (cpid == -1) {
			error_at_line(-1, errno, __FILE__, __LINE__,
					"waitpid()");
		}

		while (!WIFEXITED(status[i]) && !WIFSIGNALED(status[i])) {
			if (WIFEXITED(status[i])) {
				printf("Child %d exited with %d\n", cpid,
						WEXITSTATUS(status[i]));
			} else if (WIFSIGNALED(status[i])) {
				printf("Child %d killed by signal %d\n", cpid,
						WSTOPSIG(status[i]));
			}
		}
	}

	return 0;
}
