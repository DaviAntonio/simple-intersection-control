/*
 *  Simple Intersection Controller - simulate an intersection on a Raspberry Pi
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Simple Intersection Controller.
 *
 *  Simple Intersection Controller is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include <wiringPi.h>

#include "state_machine.h"
#include "intersection_controller.h"
#include "gpios.h"

void (*state)();
struct fsm_signal_t fsm_signal;
int wait_ms;

void set_fsm_state(void (*st)())
{
	state = st;
}

void next_state()
{
	(*state)();
}

int get_wait_ms()
{
	return wait_ms;
}

void set_wait_ms(int t)
{
	wait_ms = t;
}

void st_main_red_aux_red_0()
{
	printf("Main RED, Aux RED 0\n");
	digitalWrite(get_gpio_sem_red_1(), HIGH);
	digitalWrite(get_gpio_sem_red_2(), HIGH);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), LOW);
	digitalWrite(get_gpio_sem_green_2(), LOW);

	fsm_signal.regular = false;
	printf("fsm_signal.regular = false\n");

	fsm_signal.st_main_green_aux_red_min = false;
	printf("fsm_signal.st_main_green_aux_red_min = false\n");
	fsm_signal.st_main_red_aux_green_min = false;
	printf("fsm_signal.st_main_red_aux_green_min = false\n");

	if (fsm_signal.emergency_main)
		state = st_main_green_aux_red_emergency;
	else if (fsm_signal.emergency_aux)
		state = st_main_red_aux_green_emergency;
	else if (fsm_signal.night)
		state = st_main_yellow_aux_yellow;
	else
		state = st_main_green_aux_red_min;

	wait_ms = 1000;
	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_green_aux_red_min()
{
	printf("Main GREEN, Aux RED min\n");
	digitalWrite(get_gpio_sem_red_1(), LOW);
	digitalWrite(get_gpio_sem_red_2(), HIGH);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), HIGH);
	digitalWrite(get_gpio_sem_green_2(), LOW);

	fsm_signal.st_main_green_aux_red_min = false;
	printf("fsm_signal.st_main_green_aux_red_min = false\n");

	if (fsm_signal.emergency_main)
		state = st_main_green_aux_red_emergency;
	else if (fsm_signal.emergency_aux)
		state = st_main_yellow_aux_red;
	else if (fsm_signal.night)
		state = st_main_yellow_aux_yellow;
	else
		state = st_main_green_aux_red_max;

	wait_ms = 10000;
	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_green_aux_red_max()
{
	printf("Main GREEN, Aux RED max\n");
	digitalWrite(get_gpio_sem_red_1(), LOW);
	digitalWrite(get_gpio_sem_red_2(), HIGH);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), HIGH);
	digitalWrite(get_gpio_sem_green_2(), LOW);

	fsm_signal.st_main_green_aux_red_min = true;
	printf("fsm_signal.st_main_green_aux_red_min = true\n");

	wait_ms = 10000;

	if (fsm_signal.emergency_main) {
		wait_ms = 1;
		state = st_main_yellow_aux_red;
	} else if (fsm_signal.emergency_aux) {
		wait_ms = 1;
		state = st_main_yellow_aux_red;
	} else if (fsm_signal.night) {
		state = st_main_yellow_aux_yellow;
	} else if (fsm_signal.pedestrian_button_main || fsm_signal.car_button_aux) {
		wait_ms = 1;
		fsm_signal.pedestrian_button_main = false;
		fsm_signal.car_button_aux = false;
		printf("fsm_signal.pedestrian_button_main = false\n");
		printf("fsm_signal.car_button_aux = false\n");
		state = st_main_yellow_aux_red;
	} else {
		state = st_main_yellow_aux_red;
	}
	printf("Wait for %d ms\n\n", wait_ms);
}


void st_main_yellow_aux_red()
{
	printf("Main YELLOW, Aux RED\n");
	digitalWrite(get_gpio_sem_red_1(), LOW);
	digitalWrite(get_gpio_sem_red_2(), HIGH);
	digitalWrite(get_gpio_sem_yellow_1(), HIGH);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), LOW);
	digitalWrite(get_gpio_sem_green_2(), LOW);

	state = st_main_red_aux_red_1;
	wait_ms = 3000;

	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_red_aux_red_1()
{
	printf("Main RED, Aux RED 1\n");
	digitalWrite(get_gpio_sem_red_1(), HIGH);
	digitalWrite(get_gpio_sem_red_2(), HIGH);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), LOW);
	digitalWrite(get_gpio_sem_green_2(), LOW);

	fsm_signal.st_main_green_aux_red_min = false;
	printf("fsm_signal.st_main_green_aux_red_min = false\n");
	fsm_signal.st_main_red_aux_green_min = false;
	printf("fsm_signal.st_main_red_aux_green_min = false\n");

	if (fsm_signal.emergency_main)
		state = st_main_green_aux_red_emergency;
	else if (fsm_signal.emergency_aux)
		state = st_main_red_aux_green_emergency;
	else if (fsm_signal.night)
		state = st_main_yellow_aux_yellow;
	else
		state = st_main_red_aux_green_min;

	wait_ms = 1000;
	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_red_aux_green_min()
{
	printf("Main RED, Aux GREEN min\n");
	digitalWrite(get_gpio_sem_red_1(), HIGH);
	digitalWrite(get_gpio_sem_red_2(), LOW);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), LOW);
	digitalWrite(get_gpio_sem_green_2(), HIGH);

	fsm_signal.st_main_red_aux_green_min = false;
	printf("fsm_signal.st_main_red_aux_green_min = false\n");

	if (fsm_signal.emergency_main)
		state = st_main_red_aux_yellow;
	else if (fsm_signal.emergency_aux)
		state = st_main_red_aux_green_emergency;
	else if (fsm_signal.night)
		state = st_main_yellow_aux_yellow;
	else
		state = st_main_red_aux_green_max;

	wait_ms = 5000;
	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_red_aux_green_max()
{
	printf("Main RED, Aux GREEN max\n");
	digitalWrite(get_gpio_sem_red_1(), HIGH);
	digitalWrite(get_gpio_sem_red_2(), LOW);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), LOW);
	digitalWrite(get_gpio_sem_green_2(), HIGH);

	fsm_signal.st_main_red_aux_green_min = true;
	printf("fsm_signal.st_main_red_aux_green_min = true\n");

	wait_ms = 5000;

	if (fsm_signal.emergency_main) {
		wait_ms = 1;
		state = st_main_red_aux_yellow;
	} else if (fsm_signal.emergency_aux) {
		wait_ms = 1;
		state = st_main_red_aux_green_emergency;
	} else if (fsm_signal.night) {
		state = st_main_yellow_aux_yellow;
	} else if (fsm_signal.pedestrian_button_aux || fsm_signal.car_button_main) {
		wait_ms = 1;
		fsm_signal.pedestrian_button_aux = false;
		fsm_signal.car_button_main = false;
		printf("fsm_signal.pedestrian_button_aux = false\n");
		printf("fsm_signal.car_button_main = false\n");
		state = st_main_red_aux_yellow;
	} else {
		state = st_main_red_aux_yellow;
	}
	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_red_aux_yellow()
{
	printf("Main RED, Aux YELLOW\n");
	digitalWrite(get_gpio_sem_red_1(), HIGH);
	digitalWrite(get_gpio_sem_red_2(), LOW);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), HIGH);
	digitalWrite(get_gpio_sem_green_1(), LOW);
	digitalWrite(get_gpio_sem_green_2(), LOW);

	state = st_main_red_aux_red_0;
	wait_ms = 3000;
	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_yellow_aux_yellow()
{
	printf("Main YELLOW, Aux YELLOW\n");
	digitalWrite(get_gpio_sem_red_1(), LOW);
	digitalWrite(get_gpio_sem_red_2(), LOW);
	digitalWrite(get_gpio_sem_yellow_1(), HIGH);
	digitalWrite(get_gpio_sem_yellow_2(), HIGH);
	digitalWrite(get_gpio_sem_green_1(), LOW);
	digitalWrite(get_gpio_sem_green_2(), LOW);

	fsm_signal.night = false;
	printf("fsm_signal.night = false\n");

	if (fsm_signal.regular)
		state = st_main_red_aux_red_0;
	else if (fsm_signal.emergency_main)
		state = st_main_green_aux_red_emergency;
	else if (fsm_signal.emergency_aux)
		state = st_main_red_aux_green_emergency;
	else
		state = st_main_off_aux_off;

	wait_ms = 1000;
	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_off_aux_off()
{
	printf("Main OFF, Aux OFF\n");
	digitalWrite(get_gpio_sem_red_1(), LOW);
	digitalWrite(get_gpio_sem_red_2(), LOW);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), LOW);
	digitalWrite(get_gpio_sem_green_2(), LOW);

	state = st_main_yellow_aux_yellow;
	wait_ms = 1000;
	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_green_aux_red_emergency()
{
	printf("EMERGENCY Main GREEN, Aux RED\n");
	digitalWrite(get_gpio_sem_red_1(), LOW);
	digitalWrite(get_gpio_sem_red_2(), HIGH);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), HIGH);
	digitalWrite(get_gpio_sem_green_2(), LOW);

	fsm_signal.emergency_main = false;
	printf("fsm_signal.emergency_main = false\n");

	if (fsm_signal.regular)
		state = st_main_red_aux_red_0;
	else if (fsm_signal.night)
		state = st_main_yellow_aux_yellow;
	else if (fsm_signal.emergency_aux)
		state = st_main_red_aux_green_emergency;
	else
		state = st_main_green_aux_red_emergency;

	wait_ms = 1000;
	printf("Wait for %d ms\n\n", wait_ms);
}

void st_main_red_aux_green_emergency()
{
	printf("EMERGENCY Main RED, Aux GREEN\n");
	digitalWrite(get_gpio_sem_red_1(), HIGH);
	digitalWrite(get_gpio_sem_red_2(), LOW);
	digitalWrite(get_gpio_sem_yellow_1(), LOW);
	digitalWrite(get_gpio_sem_yellow_2(), LOW);
	digitalWrite(get_gpio_sem_green_1(), LOW);
	digitalWrite(get_gpio_sem_green_2(), HIGH);

	fsm_signal.emergency_aux = false;
	printf("fsm_signal.emergency_aux = false\n");

	if (fsm_signal.regular)
		state = st_main_red_aux_red_0;
	else if (fsm_signal.night)
		state = st_main_yellow_aux_yellow;
	else if (fsm_signal.emergency_main)
		state = st_main_green_aux_red_emergency;
	else
		state = st_main_red_aux_green_emergency;

	wait_ms = 1000;
	printf("Wait for %d ms\n\n", wait_ms);
}

void set_fsm_signal_1(struct intersection_event iev)
{
	switch (iev.type) {
	case PEDESTRIAN_BUTTON_MAIN:
		fsm_signal.pedestrian_button_main = true;
		printf("fsm_signal.pedestrian_button_main = true\n");
		if (fsm_signal.st_main_green_aux_red_min) {
			printf("Speed up\n");
			wait_ms = 1;
			fsm_signal.st_main_green_aux_red_min = false;
			printf("fsm_signal.st_main_green_aux_red_min = false\n");
			fsm_signal.pedestrian_button_main = false;
			printf("fsm_signal.pedestrian_button_main = false\n");
		}
		break;
	case PEDESTRIAN_BUTTON_AUX:
		fsm_signal.pedestrian_button_aux = true;
		printf("fsm_signal.pedestrian_button_aux = true\n");
		if (fsm_signal.st_main_red_aux_green_min) {
			printf("Speed up\n");
			wait_ms = 1;
			fsm_signal.st_main_red_aux_green_min = false;
			printf("fsm_signal.st_main_red_aux_green_min = false\n");
			fsm_signal.pedestrian_button_aux = false;
			printf("fsm_signal.pedestrian_button_aux = false\n");
		}
		break;
	case CAR_BUTTON_MAIN_1:
	case CAR_BUTTON_MAIN_2:
		fsm_signal.car_button_main = true;
		printf("fsm_signal.car_button_main = true\n");
		if (fsm_signal.st_main_red_aux_green_min) {
			printf("Speed up\n");
			wait_ms = 1;
			fsm_signal.st_main_red_aux_green_min = false;
			printf("fsm_signal.st_main_red_aux_green_min = false\n");
			fsm_signal.car_button_main = false;
			printf("fsm_signal.car_button_main = false\n");
		}
		break;
	case CAR_BUTTON_AUX_1:
	case CAR_BUTTON_AUX_2:
		fsm_signal.car_button_aux = true;
		printf("fsm_signal.car_button_aux = true\n");
		if (fsm_signal.st_main_green_aux_red_min) {
			printf("Speed up\n");
			wait_ms = 1;
			fsm_signal.st_main_green_aux_red_min = false;
			fsm_signal.car_button_aux = false;
		}
		break;
	case EMERGENCY_MAIN:
		fsm_signal.emergency_main = true;
		printf("fsm_signal.emergency_main = true\n");
		break;
	case EMERGENCY_AUX:
		fsm_signal.emergency_aux = true;
		printf("fsm_signal.emergency_aux = true\n");
		break;
	case NIGHT_MODE:
		fsm_signal.night = true;
		printf("fsm_signal.night = true\n");
		break;
	case REGULAR:
		fsm_signal.regular = true;
		printf("fsm_signal.regular = true\n");
		break;
	case CAR_RUN_RED_LIGHT_MAIN_1:
	case CAR_RUN_RED_LIGHT_MAIN_2:
		if ((state == st_main_red_aux_green_min)
				|| (state == st_main_red_aux_green_max)
				|| (state == st_main_red_aux_green_emergency)
				|| (state == st_main_red_aux_yellow)
				|| (state == st_main_red_aux_red_0)
				|| (state == st_main_red_aux_red_1)) {
			printf("Car ran on red light on main\n");
		}
		break;
	case CAR_RUN_RED_LIGHT_AUX_1:
	case CAR_RUN_RED_LIGHT_AUX_2:
		if ((state == st_main_green_aux_red_min)
				|| (state == st_main_green_aux_red_max)
				|| (state == st_main_green_aux_red_emergency)
				|| (state == st_main_yellow_aux_red)
				|| (state == st_main_red_aux_red_0)
				|| (state == st_main_red_aux_red_1)) {
			printf("Car ran on red light on aux\n");
		}
		break;
	case CAR_GO_AUX_1:
	case CAR_GO_AUX_2:
	case CAR_OVERSPEED_MAIN_1:
	case CAR_OVERSPEED_MAIN_2:
		printf("Not implemented\n");
		break;
	default:
		printf("Unknown event %d\n", iev.type);

	}
}

void set_fsm_signal(char c)
{
	switch (c) {
	case 'P':
		fsm_signal.pedestrian_button_main = true;
		printf("fsm_signal.pedestrian_button_main = true\n");
		if (fsm_signal.st_main_green_aux_red_min) {
			printf("Speed up\n");
			wait_ms = 1;
			fsm_signal.st_main_green_aux_red_min = false;
			printf("fsm_signal.st_main_green_aux_red_min = false\n");
			fsm_signal.pedestrian_button_main = false;
			printf("fsm_signal.pedestrian_button_main = false\n");
		}
		break;
	case 'p':
		fsm_signal.pedestrian_button_aux = true;
		printf("fsm_signal.pedestrian_button_aux = true\n");
		if (fsm_signal.st_main_red_aux_green_min) {
			printf("Speed up\n");
			wait_ms = 1;
			fsm_signal.st_main_red_aux_green_min = false;
			printf("fsm_signal.st_main_red_aux_green_min = false\n");
			fsm_signal.pedestrian_button_aux = false;
			printf("fsm_signal.pedestrian_button_aux = false\n");
		}
		break;
	case 'C':
		fsm_signal.car_button_main = true;
		printf("fsm_signal.car_button_main = true\n");
		if (fsm_signal.st_main_red_aux_green_min) {
			printf("Speed up\n");
			wait_ms = 1;
			fsm_signal.st_main_red_aux_green_min = false;
			printf("fsm_signal.st_main_red_aux_green_min = false\n");
			fsm_signal.car_button_main = false;
			printf("fsm_signal.car_button_main = false\n");
		}
		break;
	case 'c':
		fsm_signal.car_button_aux = true;
		printf("fsm_signal.car_button_aux = true\n");
		if (fsm_signal.st_main_green_aux_red_min) {
			printf("Speed up\n");
			wait_ms = 1;
			fsm_signal.st_main_green_aux_red_min = false;
			fsm_signal.car_button_aux = false;
		}
		break;
	case 'E':
		fsm_signal.emergency_main = true;
		printf("fsm_signal.emergency_main = true\n");
		break;
	case 'e':
		fsm_signal.emergency_aux = true;
		printf("fsm_signal.emergency_aux = true\n");
		break;
	case 'n':
		fsm_signal.night = true;
		printf("fsm_signal.night = true\n");
		break;
	case 'r':
		fsm_signal.regular = true;
		printf("fsm_signal.regular = true\n");
		break;
	case 'G':
		if ((state == st_main_red_aux_green_min)
				|| (state == st_main_red_aux_green_max)
				|| (state == st_main_red_aux_green_emergency)
				|| (state == st_main_red_aux_yellow)
				|| (state == st_main_red_aux_red_0)
				|| (state == st_main_red_aux_red_1)) {
			printf("Car ran on red light on main\n");
		}
		break;
	case 'g':
		if ((state == st_main_green_aux_red_min)
				|| (state == st_main_green_aux_red_max)
				|| (state == st_main_green_aux_red_emergency)
				|| (state == st_main_yellow_aux_red)
				|| (state == st_main_red_aux_red_0)
				|| (state == st_main_red_aux_red_1)) {
			printf("Car ran on red light on aux\n");
		}
		break;
	default:
		printf("Unknown event %c\n", c);
	}
}

bool is_main_red()
{
	return ((state == st_main_red_aux_green_min)
			|| (state == st_main_red_aux_green_max)
			|| (state == st_main_red_aux_green_emergency)
			|| (state == st_main_red_aux_yellow)
			|| (state == st_main_red_aux_red_0)
			|| (state == st_main_red_aux_red_1));
}

bool is_aux_red()
{
	return ((state == st_main_green_aux_red_min)
			|| (state == st_main_green_aux_red_max)
			|| (state == st_main_green_aux_red_emergency)
			|| (state == st_main_yellow_aux_red)
			|| (state == st_main_red_aux_red_0)
			|| (state == st_main_red_aux_red_1));
}
