/*
 *  Simple Intersection Controller - simulate an intersection on a Raspberry Pi
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Simple Intersection Controller.
 *
 *  Simple Intersection Controller is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h> // memset
#include <unistd.h> // sleep, *FILENO
#include <errno.h>
#include <error.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h> // inet_pton
#include <sys/epoll.h>
#include <time.h> // clock_*, nanosleep
#include <pthread.h>

#include <wiringPi.h>

#include "messages.h"
#include "state_machine.h"
#include "time_utils.h"
#include "intersection_controller.h"
#include "gpios.h"

#define MAX_EVENTS (100)

int epoll_fd;
struct epoll_event events[MAX_EVENTS];

void *watch_go_sensor_1(void *targ)
{
	struct watch_go_sensor_args *args = targ;
	struct timespec ts_rising, ts_falling, ts_now, ts_diff;
	int ms_diff;
	const int resolution_ms = 10;
	const struct timespec resolution = ms2timespec(resolution_ms);
	const int stop_threshold_ms = 2000;
	int previous_state = 0;
	int actual_state = previous_state;
	bool suppress_go_msg = false;

	printf("Watching sensor %d\n", args->gpio);

	while (1) {
		actual_state = digitalRead(args->gpio);
		/*printf("previous: %d actual: %d\n", previous_state,
				actual_state);*/
		if ((previous_state == 0) && (actual_state == 1)) {
			// rising edge
			printf("Rising edge\n");
			clock_gettime(CLOCK_BOOTTIME, &ts_rising);
			suppress_go_msg = false;
		} else if ((previous_state == 1) && (actual_state == 0)) {
			// falling edge
			printf("Falling edge\n");
			clock_gettime(CLOCK_BOOTTIME, &ts_falling);
			struct intersection_event msg;
			char smsg[MSG_LEN];
			int smsg_len;
			memset(smsg, 0, MSG_LEN);
			msg.type = CAR_GO_AUX_1;
			msg.time = ts_falling;
			serialise_msg(msg, smsg, &smsg_len);
			write(args->fd_to_write, smsg, smsg_len);
		} else if ((previous_state == 1) && (actual_state == 1)) {
			clock_gettime(CLOCK_BOOTTIME, &ts_now);
			timespec_diff(&ts_now, &ts_rising, &ts_diff);
			ms_diff = timespec_ms(ts_diff);
			printf("Time since last rising %d\n", ms_diff);
			if ((ms_diff > stop_threshold_ms) && !suppress_go_msg) {
				printf("Too long\n");
				struct intersection_event msg;
				char smsg[MSG_LEN];
				int smsg_len;
				memset(smsg, 0, MSG_LEN);
				msg.type = CAR_BUTTON_AUX_1;
				msg.time = ts_now;
				serialise_msg(msg, smsg, &smsg_len);
				write(args->fd_to_write, smsg, smsg_len);
				suppress_go_msg = true;
			}
		}
		previous_state = actual_state;
		nanosleep(&resolution, NULL);
	}
}

void *watch_go_sensor_2(void *targ)
{
	struct watch_go_sensor_args *args = targ;
	struct timespec ts_rising, ts_falling, ts_now, ts_diff;
	int ms_diff;
	const int resolution_ms = 10;
	const struct timespec resolution = ms2timespec(resolution_ms);
	const int stop_threshold_ms = 2000;
	int previous_state = 0;
	int actual_state = previous_state;
	bool suppress_go_msg = false;

	printf("Watching sensor %d\n", args->gpio);

	while (1) {
		actual_state = digitalRead(args->gpio);
		/*printf("previous: %d actual: %d\n", previous_state,
				actual_state);*/
		if ((previous_state == 0) && (actual_state == 1)) {
			// rising edge
			printf("Rising edge\n");
			clock_gettime(CLOCK_BOOTTIME, &ts_rising);
			suppress_go_msg = false;
		} else if ((previous_state == 1) && (actual_state == 0)) {
			// falling edge
			printf("Falling edge\n");
			clock_gettime(CLOCK_BOOTTIME, &ts_falling);
			struct intersection_event msg;
			char smsg[MSG_LEN];
			int smsg_len;
			memset(smsg, 0, MSG_LEN);
			msg.type = CAR_GO_AUX_2;
			msg.time = ts_falling;
			serialise_msg(msg, smsg, &smsg_len);
			write(args->fd_to_write, smsg, smsg_len);
		} else if ((previous_state == 1) && (actual_state == 1)) {
			clock_gettime(CLOCK_BOOTTIME, &ts_now);
			timespec_diff(&ts_now, &ts_rising, &ts_diff);
			ms_diff = timespec_ms(ts_diff);
			printf("Time since last rising %d\n", ms_diff);
			if ((ms_diff > stop_threshold_ms) && !suppress_go_msg) {
				printf("Too long\n");
				struct intersection_event msg;
				char smsg[MSG_LEN];
				int smsg_len;
				memset(smsg, 0, MSG_LEN);
				msg.type = CAR_BUTTON_AUX_2;
				msg.time = ts_now;
				serialise_msg(msg, smsg, &smsg_len);
				write(args->fd_to_write, smsg, smsg_len);
				suppress_go_msg = true;
			}
		}
		previous_state = actual_state;
		nanosleep(&resolution, NULL);
	}

}

void *watch_pedestrian_sensor_main(void *targ)
{
	struct watch_go_sensor_args *args = targ;
	const int resolution_ms = 100;
	const struct timespec resolution = ms2timespec(resolution_ms);
	int previous_state = 0;
	int actual_state = previous_state;
	bool suppress_go_msg = false;

	printf("Watching sensor %d\n", args->gpio);

	while (1) {
		actual_state = digitalRead(args->gpio);
		if ((previous_state == 0) && (actual_state == 1)) {
			// rising edge
			printf("Rising edge\n");
			if (!suppress_go_msg) {
				struct intersection_event msg;
				char smsg[MSG_LEN];
				int smsg_len;
				memset(smsg, 0, MSG_LEN);
				msg.type = PEDESTRIAN_BUTTON_MAIN;
				serialise_msg(msg, smsg, &smsg_len);
				write(args->fd_to_write, smsg, smsg_len);
			}
			suppress_go_msg = true;
		} else if ((previous_state == 1) && (actual_state == 0)) {
			// falling edge
			printf("Falling edge\n");
			suppress_go_msg = false;
		}
		previous_state = actual_state;
		nanosleep(&resolution, NULL);
	}

}

void *watch_pedestrian_sensor_aux(void *targ)
{
	struct watch_go_sensor_args *args = targ;
	const int resolution_ms = 100;
	const struct timespec resolution = ms2timespec(resolution_ms);
	int previous_state = 0;
	int actual_state = previous_state;
	bool suppress_go_msg = false;

	printf("Watching sensor %d\n", args->gpio);

	while (1) {
		actual_state = digitalRead(args->gpio);
		if ((previous_state == 0) && (actual_state == 1)) {
			// rising edge
			printf("Rising edge\n");
			if (!suppress_go_msg) {
				struct intersection_event msg;
				char smsg[MSG_LEN];
				int smsg_len;
				memset(smsg, 0, MSG_LEN);
				msg.type = PEDESTRIAN_BUTTON_AUX;
				serialise_msg(msg, smsg, &smsg_len);
				write(args->fd_to_write, smsg, smsg_len);
			}
			suppress_go_msg = true;
		} else if ((previous_state == 1) && (actual_state == 0)) {
			// falling edge
			printf("Falling edge\n");
			suppress_go_msg = false;
		}
		previous_state = actual_state;
		nanosleep(&resolution, NULL);
	}

}

void *watch_speed_sensor(void *targ)
{
	struct watch_speed_sensor_args *args = targ;
}

void validate_gpio(int scanf_result, int arg_index, int value)
{
	if (scanf_result != 1) {
		printf("Argument %d is not a number!\n", arg_index);
		exit(EXIT_FAILURE);
	} else {
		if (value < 0) {
			printf("Argument %d is negative\n", arg_index);
			exit(EXIT_FAILURE);
		}
	}
}

void process_events(int ready_fds, int srv_sock)
{
	for (int i = 0; i < ready_fds; i++) {
		if (events[i].data.fd == STDIN_FILENO) {
			if (events[i].events & EPOLLIN) {
				char c;
				scanf(" %c", &c);
				set_fsm_signal(c);
			}
		} else if (events[i].data.fd == srv_sock) {
			printf("server\n");
			// events
			if ((events[i].events & EPOLLRDHUP)
					|| (events[i].events & EPOLLHUP)) {
				printf("Server has disconnected\n");
				struct intersection_event iev;
				int result;

				iev.type = NIGHT_MODE;
				set_fsm_signal_1(iev);
				printf("Emergencial night mode enabled\n");

				result = epoll_ctl(epoll_fd, EPOLL_CTL_DEL,
						events[i].data.fd, NULL);
				if (result == -1) {
					error_at_line(-1, errno, __FILE__,
							__LINE__, "epoll_ctl()");
				}
				printf("Please restart\n");
			} else if (events[i].events & EPOLLIN) {
				printf("read\n");
				char buf[MSG_LEN];
				memset(buf, 0, MSG_LEN);
				recv(events[i].data.fd, buf, MSG_LEN, 0);
				printf("Got %" MSG_LEN_STR "s\n", buf);

				struct intersection_event iev;
				iev = deserialise(buf);
				set_fsm_signal_1(iev);
			}
		} else {
			// resend if possible
			printf("A pipe\n");
			char buf[MSG_LEN];
			int buf_len;
			int result;
			memset(buf, 0, MSG_LEN);
			read(events[i].data.fd, buf, MSG_LEN);
			printf("Got %" MSG_LEN_STR "s\n", buf);

			struct intersection_event iev;
			iev = deserialise(buf);
			set_fsm_signal_1(iev);

			if ((iev.type == CAR_GO_AUX_1) && is_aux_red()) {
				printf("Car ran on red light on aux 1\n");
				iev.type = CAR_RUN_RED_LIGHT_AUX_1;
			}

			if ((iev.type == CAR_GO_AUX_2) && is_aux_red()) {
				printf("Car ran on red light on aux 2\n");
				iev.type = CAR_RUN_RED_LIGHT_AUX_2;
			}

			memset(buf, 0, MSG_LEN);
			serialise_msg(iev, buf, &buf_len);
			result = send(srv_sock, buf, buf_len, 0);
			if (result == buf_len) {
				printf("Sent %" MSG_LEN_STR "s with %d bytes\n",
						buf, buf_len);
			} else {
				printf("Sent incomplete message\n");
			}

		}
	}
}

int main(int argc, char **argv)
{
	int srv_sock;
	int go_sensor_1_pipe[2];
	int go_sensor_2_pipe[2];
	int pedestrian_main_pipe[2];
	int pedestrian_aux_pipe[2];
	struct sockaddr_in srv_addr;
	struct epoll_event event;
	int ready_events, ms_diff;
	struct timespec before, after, ts_diff;
	int result;
	struct watch_go_sensor_args go_sensor_1_args;
	struct watch_go_sensor_args go_sensor_2_args;
	struct watch_go_sensor_args pedestrian_main_args;
	struct watch_go_sensor_args pedestrian_aux_args;

	pthread_t go_sensor_1_thread;
	pthread_t go_sensor_2_thread;
	pthread_t pedestrian_main_thread;
	pthread_t pedestrian_aux_thread;

	if (argc != 17) {
		printf("%s server_ip server_port gpio_sem_red_1 gpio_sem_red_2 gpio_sem_yellow_1 gpio_sem_yellow_2 gpio_sem_green_1 gpio_sem_green_2 gpio_pedestrian_button_1 gpio_pedestrian_button_2 gpio_go_sensor_1 gpio_go_sensor_2 gpio_speed_sensor_1_a gpio_speed_sensor_1_b gpio_speed_sensor_2_a gpio_speed_sensor_2_b\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	// gpios
	int gpio_sem_red_1 = -1;
	int gpio_sem_red_2 = -1;
	int gpio_sem_yellow_1 = -1;
	int gpio_sem_yellow_2 = -1;
	int gpio_sem_green_1 = -1;
	int gpio_sem_green_2 = -1;
	int gpio_pedestrian_button_1 = -1;
	int gpio_pedestrian_button_2 = -1;
	int gpio_go_sensor_1 = -1;
	int gpio_go_sensor_2 = -1;
	int gpio_speed_sensor_1_a = -1;
	int gpio_speed_sensor_1_b = -1;
	int gpio_speed_sensor_2_a = -1;
	int gpio_speed_sensor_2_b = -1;

	result = sscanf(argv[3], " %d", &gpio_sem_red_1);
	validate_gpio(result, 3, gpio_sem_red_1);
	result = sscanf(argv[4], " %d", &gpio_sem_red_2);
	validate_gpio(result, 4, gpio_sem_red_2);
	result = sscanf(argv[5], " %d", &gpio_sem_yellow_1);
	validate_gpio(result, 5, gpio_sem_yellow_1);
	result = sscanf(argv[6], " %d", &gpio_sem_yellow_2);
	validate_gpio(result, 6, gpio_sem_yellow_2);
	result = sscanf(argv[7], " %d", &gpio_sem_green_1);
	validate_gpio(result, 7, gpio_sem_green_1);
	result = sscanf(argv[8], " %d", &gpio_sem_green_2);
	validate_gpio(result, 8, gpio_sem_green_2);
	result = sscanf(argv[9], " %d", &gpio_pedestrian_button_1);
	validate_gpio(result, 9, gpio_pedestrian_button_1);
	result = sscanf(argv[10], " %d", &gpio_pedestrian_button_2);
	validate_gpio(result, 10, gpio_pedestrian_button_2);
	result = sscanf(argv[11], " %d", &gpio_go_sensor_1);
	validate_gpio(result, 11, gpio_go_sensor_1);
	result = sscanf(argv[12], " %d", &gpio_go_sensor_2);
	validate_gpio(result, 12, gpio_go_sensor_2);
	result = sscanf(argv[13], " %d", &gpio_speed_sensor_1_a);
	validate_gpio(result, 13, gpio_speed_sensor_1_a);
	result = sscanf(argv[14], " %d", &gpio_speed_sensor_1_b);
	validate_gpio(result, 14, gpio_speed_sensor_1_b);
	result = sscanf(argv[15], " %d", &gpio_speed_sensor_2_a);
	validate_gpio(result, 15, gpio_speed_sensor_2_a);
	result = sscanf(argv[16], " %d", &gpio_speed_sensor_2_b);
	validate_gpio(result, 16, gpio_speed_sensor_2_b);

	set_gpio_sem_red_1(gpio_sem_red_1);
	set_gpio_sem_red_2(gpio_sem_red_2);
	set_gpio_sem_yellow_1(gpio_sem_yellow_1);
	set_gpio_sem_yellow_2(gpio_sem_yellow_2);
	set_gpio_sem_green_1(gpio_sem_green_1);
	set_gpio_sem_green_2(gpio_sem_green_2);
	set_gpio_pedestrian_button_1(gpio_pedestrian_button_1);
	set_gpio_pedestrian_button_2(gpio_pedestrian_button_2);
	set_gpio_go_sensor_1(gpio_go_sensor_1);
	set_gpio_go_sensor_2(gpio_go_sensor_2);
	set_gpio_speed_sensor_1_a(gpio_speed_sensor_1_a);
	set_gpio_speed_sensor_1_b(gpio_speed_sensor_1_b);
	set_gpio_speed_sensor_2_a(gpio_speed_sensor_2_a);
	set_gpio_speed_sensor_2_b(gpio_speed_sensor_2_b);
	set_gpio_sem_red_1(gpio_sem_red_1);

	// setup WiringPi with BCM numbers
	wiringPiSetupGpio();

	pinMode(gpio_sem_red_1, OUTPUT);
	pinMode(gpio_sem_red_2, OUTPUT);
	pinMode(gpio_sem_yellow_1, OUTPUT);
	pinMode(gpio_sem_yellow_2, OUTPUT);
	pinMode(gpio_sem_green_1, OUTPUT);
	pinMode(gpio_sem_green_2, OUTPUT);

	pinMode(gpio_go_sensor_1, INPUT);
	pinMode(gpio_go_sensor_2, INPUT);

	pinMode(gpio_pedestrian_button_1, INPUT);
	pinMode(gpio_pedestrian_button_2, INPUT);

	pinMode(gpio_speed_sensor_1_a, INPUT);
	pinMode(gpio_speed_sensor_1_b, INPUT);
	pinMode(gpio_speed_sensor_2_a, INPUT);
	pinMode(gpio_speed_sensor_2_b, INPUT);

	// outputs to low
	digitalWrite(gpio_sem_red_1, LOW);
	digitalWrite(gpio_sem_red_2, LOW);
	digitalWrite(gpio_sem_yellow_1, LOW);
	digitalWrite(gpio_sem_yellow_2, LOW);
	digitalWrite(gpio_sem_green_1, LOW);
	digitalWrite(gpio_sem_green_2, LOW);

	// server's address
	memset(&srv_addr, 0, sizeof(srv_addr));
	srv_addr.sin_family = AF_INET;
	result = inet_pton(AF_INET, argv[1], &(srv_addr.sin_addr));
	if (result <= 0) {
		if (!result) {
			fprintf(stderr, "'%s' is not a valid address\n",
					argv[1]);
			exit(EXIT_FAILURE);
		} else {
			error_at_line(-1, errno, __FILE__, __LINE__,
					"inet_pton()");
		}
	}
	if (sscanf(argv[2], "%hu", &(srv_addr.sin_port)) != 1) {
		fprintf(stderr, "'%s' is an invalid port\n", argv[2]);
		exit(EXIT_FAILURE);
	}

	// TCP socket
	srv_sock = socket(AF_INET, SOCK_STREAM, 0);

	if (srv_sock == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__, "socket()");
	}

	// try to connect to server
	if (connect(srv_sock, (const struct sockaddr*) &srv_addr,
				sizeof(srv_addr)) == -1) {
		error_at_line(-2, errno, __FILE__, __LINE__, "connect()");
	}

	printf("Connected!\n");

	set_wait_ms(0);

	//state = st_main_yellow_aux_yellow;
	set_fsm_state(&st_main_yellow_aux_yellow);

	// watch stdin input
	epoll_fd = epoll_create1(0);
	memset(&event, 0, sizeof(event));
	event.data.fd = STDIN_FILENO;
	event.events = EPOLLIN;
	epoll_ctl(epoll_fd, EPOLL_CTL_ADD, STDIN_FILENO, &event);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__, "epoll_ctl(stdin)");
	}

	// watch for server instructions
	memset(&event, 0, sizeof(event));
	event.data.fd = srv_sock;
	event.events = EPOLLIN | EPOLLHUP | EPOLLRDHUP;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, srv_sock, &event);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__, "epoll_ctl(srv_sock)");
	}

	// preparing pipes and threads
	result = pipe(go_sensor_1_pipe);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"pipe(go_sensor_1_pipe)");
	}

	result = pipe(go_sensor_2_pipe);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"pipe(go_sensor_2_pipe)");
	}

	result = pipe(pedestrian_main_pipe);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"pipe(pedestrian_main_pipe)");
	}

	result = pipe(pedestrian_aux_pipe);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"pipe(pedestrian_aux_pipe)");
	}

	go_sensor_1_args.fd_to_write = go_sensor_1_pipe[1];
	go_sensor_1_args.gpio = gpio_go_sensor_1;

	go_sensor_2_args.fd_to_write = go_sensor_2_pipe[1];
	go_sensor_2_args.gpio = gpio_go_sensor_2;

	pedestrian_main_args.fd_to_write = pedestrian_main_pipe[1];
	pedestrian_main_args.gpio = gpio_pedestrian_button_1;

	pedestrian_aux_args.fd_to_write = pedestrian_aux_pipe[1];
	pedestrian_aux_args.gpio = gpio_pedestrian_button_2;

	// prepare events for pipes
	memset(&event, 0, sizeof(event));
	event.data.fd = go_sensor_1_pipe[0];
	event.events = EPOLLIN;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, go_sensor_1_pipe[0],
			&event);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"epoll_ctl(go_sensor_1_pipe)");
	}

	memset(&event, 0, sizeof(event));
	event.data.fd = go_sensor_2_pipe[0];
	event.events = EPOLLIN;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, go_sensor_2_pipe[0],
			&event);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"epoll_ctl(go_sensor_2_pipe)");
	}

	memset(&event, 0, sizeof(event));
	event.data.fd = pedestrian_main_pipe[0];
	event.events = EPOLLIN;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, pedestrian_main_pipe[0],
			&event);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"epoll_ctl(pedestrian_main_pipe)");
	}

	memset(&event, 0, sizeof(event));
	event.data.fd = pedestrian_aux_pipe[0];
	event.events = EPOLLIN;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, pedestrian_aux_pipe[0],
			&event);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"epoll_ctl(pedestrian_aux_pipe)");
	}

	// prepare threads
	result = pthread_create(&go_sensor_1_thread, 0, watch_go_sensor_1,
			&go_sensor_1_args);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"pthread_create(go_sensor_1)");
	}

	result = pthread_create(&go_sensor_2_thread, 0, watch_go_sensor_2,
			&go_sensor_2_args);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"pthread_create(go_sensor_2)");
	}

	result = pthread_create(&pedestrian_main_thread, 0, watch_pedestrian_sensor_main,
			&pedestrian_main_args);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"pthread_create(pedestrian_sensor_main)");
	}

	result = pthread_create(&pedestrian_aux_thread, 0, watch_pedestrian_sensor_aux,
			&pedestrian_aux_args);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"pthread_create(pedestrian_sensor_aux)");
	}

	while (1) {
		clock_gettime(CLOCK_BOOTTIME, &before);
		ready_events = epoll_wait(epoll_fd, events, MAX_EVENTS, get_wait_ms());
		if (ready_events == 0) {
			printf("%d ms elapsed?\n", get_wait_ms());
		} else {
			process_events(ready_events, srv_sock);
		}

		clock_gettime(CLOCK_BOOTTIME, &after);

		timespec_diff(&after, &before, &ts_diff);
		ms_diff = timespec_ms(ts_diff);
		if (get_wait_ms() > ms_diff) {
			struct timespec extra = ms2timespec(get_wait_ms() - ms_diff);
			printf("Must sleep extra %d ms\n", get_wait_ms() - ms_diff);
			nanosleep(&extra, NULL);
		}

		next_state();
	}
	return 0;
}
