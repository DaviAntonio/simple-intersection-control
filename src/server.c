/*
 *  Simple Intersection Controller - simulate an intersection on a Raspberry Pi
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Simple Intersection Controller.
 *
 *  Simple Intersection Controller is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h> // *_FILENO
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h> // inet_pton
#include <errno.h>
#include <error.h>
#include <sys/epoll.h>

#include "messages.h"

#define MAX_EVENTS (100)

enum commands {
	PRINT_STATS = 0,
	SEND_REGULAR,
	SEND_NIGHT,
	SEND_EMERGENCY_MAIN,
	SEND_EMERGENCY_AUX
};

struct intersection {
	int fd;
	struct sockaddr_in addr;
	int cars_main_1;
	int cars_main_2;
	int cars_aux_1;
	int cars_aux_2;
	int cars_run_red_main_1;
	int cars_run_red_main_2;
	int cars_run_red_aux_1;
	int cars_run_red_aux_2;
	int cars_overspeed_main_1;
	int cars_overspeed_main_2;
	char smsg_rx[MSG_LEN];
	bool to_send;
	bool valid;
	char smsg_tx[MSG_LEN];
	int smsg_tx_len;
};

struct intersections {
	int size;
	struct intersection *intersections;
};

struct intersections all_intersections;

int epoll_fd;
struct epoll_event events[MAX_EVENTS];

void server_loop(int);
void print_commands();
void run_command(int);
int add_intersection(struct intersection);
int find_intersection_by_addr(struct sockaddr_in, struct intersection **);
int find_intersection_by_fd(int, struct intersection **);
void print_intersections();
void print_intersection(struct intersection, int);
void prepare_to_send(struct intersection *, struct intersection_event);
struct intersection *prompt_free_intersections();
void try_send(int);

void try_send(int fd)
{
	struct intersection *isec;
	int result;
	result = find_intersection_by_fd(fd, &isec);
	if (result == 0) {
		if (isec->to_send) {
			result = send(isec->fd, &(isec->smsg_tx),
					isec->smsg_tx_len, 0);
			if (result == isec->smsg_tx_len) {
				printf("Successfully sent %d bytes\n", result);
			} else {
				printf("Failed to send the full message\n");
			}
		} else {
			printf("nothing to be sent\n");
		}
	} else {
		printf("Impossible to find the intersection\n");
	}

	isec->to_send = false;
	struct epoll_event ev;
	ev.data.fd = fd;
	ev.events = EPOLLIN | EPOLLRDHUP | EPOLLHUP;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_MOD, fd, &ev);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__, "epoll_ctl(fd)");
	}
}

void prepare_to_send(struct intersection *isec, struct intersection_event iev)
{
	struct epoll_event ev;
	int result;

	serialise_msg(iev, isec->smsg_tx, &(isec->smsg_tx_len));
	isec->to_send = true;

	ev.data.fd = isec->fd;
	ev.events = EPOLLIN | EPOLLOUT | EPOLLRDHUP | EPOLLHUP;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_MOD, isec->fd, &ev);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__, "epoll_ctl(isec->fd)");
	}
}


struct intersection *prompt_free_intersections()
{
	int result;
	int index;

	if (all_intersections.size == 0) {
		printf("No intersections connected\n");
		return NULL;
	}
	printf("Select an intersection:\n");

	for (int i = 0; i < all_intersections.size; i++) {
		if (!all_intersections.intersections[i].to_send
				&& all_intersections.intersections[i].valid) {
			printf("%d\n", i);
		}
	}

	result = scanf(" %d", &index);
	if (result != 1 || index < 0 || index >= all_intersections.size) {
		return NULL;
	} else {
		if (!all_intersections.intersections[index].to_send
				&& all_intersections.intersections[index].valid) {
			return &(all_intersections.intersections[index]);
		} else {
			return NULL;
		}
	}
}

void print_intersection(struct intersection isec, int i)
{
	char addr[INET_ADDRSTRLEN];
	if (inet_ntop(AF_INET, &(isec.addr.sin_addr), addr, INET_ADDRSTRLEN) == NULL) {
		error_at_line(0, errno, __FILE__, __LINE__, "inet_ntop()");
	}
	printf("Intersection %d\n", i);
	printf("IP: %" XSTR(INET_ADDRSTRLEN) "s\n", addr);
	printf("Port: %u\n", ntohs(isec.addr.sin_port));
	printf("Status: %s\n", isec.valid ? "VALID" : "INVALID");
	printf("Cars main 1: %d\n", isec.cars_main_1);
	printf("Cars main 2: %d\n", isec.cars_main_2);
	printf("Cars aux 1: %d\n", isec.cars_aux_1);
	printf("Cars aux 2: %d\n", isec.cars_aux_2);
	printf("Cars run red main 1: %d\n", isec.cars_run_red_main_1);
	printf("Cars run red main 2: %d\n", isec.cars_run_red_main_2);
	printf("Cars run red aux 1: %d\n", isec.cars_run_red_aux_1);
	printf("Cars run red aux 2: %d\n", isec.cars_run_red_aux_2);
	printf("Cars overspeed main 1: %d\n", isec.cars_overspeed_main_1);
	printf("Cars overspeed main 2: %d\n", isec.cars_overspeed_main_2);
	if (isec.to_send) {
		printf("There are messages to be sent:\n");
		printf("Size: %d bytes\n", isec.smsg_tx_len);
		printf("%" MSG_LEN_STR "s\n", isec.smsg_tx);
	}
}

void print_intersections()
{
	if (all_intersections.size == 0)
		printf("No intersections connected\n");

	for (int i = 0; i < all_intersections.size; i++) {
		print_intersection(all_intersections.intersections[i], i);
		printf("\n");
	}
}

void run_command(int cmd)
{
	struct intersection_event msg;
	struct intersection *to_write;

	switch (cmd) {
	case PRINT_STATS:
		print_intersections();
		break;
	case SEND_REGULAR:
		msg.type = REGULAR;
		to_write = prompt_free_intersections();
		if (to_write == NULL) {
			if (all_intersections.size != 0)
				printf("Invalid intersection\n");
		} else {
			prepare_to_send(to_write, msg);
		}
		break;
	case SEND_NIGHT:
		msg.type = NIGHT_MODE;
		to_write = prompt_free_intersections();
		if (to_write == NULL) {
			if (all_intersections.size != 0)
				printf("Invalid intersection\n");
		} else {
			prepare_to_send(to_write, msg);
		}
		break;
	case SEND_EMERGENCY_MAIN:
		msg.type = EMERGENCY_MAIN;
		to_write = prompt_free_intersections();
		if (to_write == NULL) {
			if (all_intersections.size != 0)
				printf("Invalid intersection\n");
		} else {
			prepare_to_send(to_write, msg);
		}
		break;
	case SEND_EMERGENCY_AUX:
		msg.type = EMERGENCY_AUX;
		to_write = prompt_free_intersections();
		if (to_write == NULL) {
			if (all_intersections.size != 0)
				printf("Invalid intersection\n");
		} else {
			prepare_to_send(to_write, msg);
		}
		break;
	default:
		printf("Unknown command %d\n", cmd);
		print_commands();
	}
}

void print_commands()
{
	printf("%d - Print stats\n", PRINT_STATS);
	printf("%d - Send regular mode\n", SEND_REGULAR);
	printf("%d - Send night mode\n", SEND_NIGHT);
	printf("%d - Send main emergency\n", SEND_EMERGENCY_MAIN);
	printf("%d - Send aux emergency\n", SEND_EMERGENCY_AUX);
}

int add_intersection(struct intersection isec)
{
	struct intersection *t = all_intersections.intersections;
	int tsize = all_intersections.size;
	tsize++;
	t = realloc(all_intersections.intersections, tsize * sizeof(*t));

	if (t == NULL) {
		return -1;
	} else {
		all_intersections.intersections = t;
		all_intersections.size = tsize;
		all_intersections.intersections[tsize - 1] = isec;
		return 0;
	}
}

int find_intersection_by_addr(struct sockaddr_in addr, struct intersection **isec)
{
	for (int i = 0; i < all_intersections.size; i++) {
		if (memcmp(&addr, &all_intersections.intersections[i].addr,
					sizeof(addr)) == 0) {
			*isec = &(all_intersections.intersections[i]);
			return 0;
		}
	}

	return -1;
}

int find_intersection_by_fd(int fd, struct intersection **isec)
{
	for (int i = 0; i < all_intersections.size; i++) {
		if (fd == all_intersections.intersections[i].fd) {
			*isec = &(all_intersections.intersections[i]);
			return 0;
		}
	}

	return -1;
}

void server_loop(int server_sock)
{
	struct sockaddr_in client_addr;
	socklen_t client_addr_len = sizeof(client_addr);
	int ready_fds;
	int client_sock;
	int result;
	bool stop = false;

	while (!stop) {
		// block forever until event or signal
		ready_fds = epoll_wait(epoll_fd, events, MAX_EVENTS, -1);

		if (ready_fds == -1) {
			error_at_line(-1, errno, __FILE__, __LINE__,
					"epoll_wait()");
		}

		printf("\nMust process %d event(s) (max %u)\n", ready_fds,
				MAX_EVENTS);

		// which event is ready?
		for (int i = 0; i < ready_fds; i++) {
			// the server socket woke up, so a client is ready
			if (events[i].data.fd == server_sock) {
				char addr[INET_ADDRSTRLEN];
				client_sock = accept(server_sock,
					(struct sockaddr *) &client_addr,
					&client_addr_len);
				if (client_sock == -1) {
					error_at_line(0, errno, __FILE__,
							__LINE__, "accept()");
				}
				if (inet_ntop(AF_INET, &(client_addr.sin_addr),
							addr,
							INET_ADDRSTRLEN) == NULL) {
					error_at_line(0, errno, __FILE__,
							__LINE__,
							"inet_ntop()");
				}
				printf("\nClient %s:%u connected\n", addr,
						ntohs(client_addr.sin_port));

				struct intersection new_i;
				memset(&new_i, 0, sizeof(new_i));
				new_i.addr = client_addr;
				new_i.fd = client_sock;
				new_i.to_send = false;
				new_i.valid = true;
				add_intersection(new_i);

				printf("Intersections: %d\n", all_intersections.size);

				struct epoll_event event;
				event.data.fd = new_i.fd;
				event.events = EPOLLIN | EPOLLRDHUP | EPOLLHUP;
				result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD,
						new_i.fd, &event);
				if (result == -1) {
					error_at_line(-1, errno, __FILE__,
							__LINE__,
							"epoll_ctl()");
				}
			} else if (events[i].data.fd == STDIN_FILENO) {
				printf("stdin\n");
				int cmd;
				int result;
				result = scanf(" %d", &cmd);
				if (result == EOF) {
					printf("Terminating\n");
					stop = true;
				} else if (result == 1) {
					printf("Command: %d\n", cmd);
					run_command(cmd);
				}
			} else {
				printf("client\n");
				if ((events[i].events & EPOLLRDHUP)
						|| (events[i].events & EPOLLHUP)) {
					struct intersection *isec;
					int result;
					printf("Something died\n");
					result = find_intersection_by_fd(events[i].data.fd,
							&isec);
					if (result == 0) {
						printf("Intersection dead\n");
						isec->valid = false;
					}

					struct epoll_event event;
					event.data.fd = events[i].data.fd;
					result = epoll_ctl(epoll_fd, EPOLL_CTL_DEL,
							events[i].data.fd, &event);
					if (result == -1) {
						error_at_line(-1, errno, __FILE__,
							__LINE__,
							"epoll_ctl()");
					}
				} else if (events[i].events & EPOLLOUT) {
					printf("Available for writing\n");
					try_send(events[i].data.fd);
				} else if (events[i].events & EPOLLIN) {
					printf("Available for reading\n");
					char buf[MSG_LEN];
					memset(buf, 0, MSG_LEN);
					recv(events[i].data.fd, buf, MSG_LEN, 0);
					printf("Got %" MSG_LEN_STR "s\n", buf);

					struct intersection_event iev;
					iev = deserialise(buf);

					struct intersection *isec;
					int result;

					result = find_intersection_by_fd(events[i].data.fd,
							&isec);

					if ((iev.type == CAR_RUN_RED_LIGHT_AUX_1)
							&& (result == 0)) {
						isec->cars_run_red_aux_1++;
					}

					if ((iev.type == CAR_RUN_RED_LIGHT_AUX_2)
							&& (result == 0)) {
						isec->cars_run_red_aux_2++;
					}

					if ((iev.type == CAR_GO_AUX_1)
							&& (result == 0)) {
						isec->cars_aux_1++;
					}

					if ((iev.type == CAR_GO_AUX_2)
							&& (result == 0)) {
						isec->cars_aux_2++;
					}
				}
			}
		}
	}
}

int main(int argc, char **argv)
{
	int srv_sock;
	struct sockaddr_in srv_addr;
	int opt;
	int result;

	if (argc != 3) {
		printf("%s server_ip server_port\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	// server's address
	memset(&srv_addr, 0, sizeof(srv_addr));
	srv_addr.sin_family = AF_INET;
	result = inet_pton(AF_INET, argv[1], &(srv_addr.sin_addr));
	if (result <= 0) {
		if (!result) {
			fprintf(stderr, "'%s' is not a valid address\n",
					argv[1]);
			exit(EXIT_FAILURE);
		} else {
			error_at_line(-1, errno, __FILE__, __LINE__,
					"inet_pton()");
		}
	}
	if (sscanf(argv[2], "%hu", &(srv_addr.sin_port)) != 1) {
		fprintf(stderr, "'%s' is an invalid port\n", argv[2]);
		exit(EXIT_FAILURE);
	}

	srv_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (srv_sock == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__, "socket()");
	}

	/* We wish to reuse the socket
	 * SO_REUSEADDR allows the validation of the address to be bound to the
	 * socket to reuse local addresses without waiting for a timeout
	 * All sockets from AF_INET (TCP, UDP) will be able to reuse the address
	 * unless there is a listening socket bound to it
	 */
	opt = 1;
	if (setsockopt(srv_sock, SOL_SOCKET, SO_REUSEADDR, &opt,
				sizeof(opt)) == -1) {
		error_at_line(-2, errno, __FILE__, __LINE__,
				"setsockpt(SO_REUSEADDR)");
	}

	// bind address to socket
	if (bind(srv_sock, (const struct sockaddr *) &srv_addr,
				sizeof(srv_addr)) == -1) {
		error_at_line(-2, errno, __FILE__, __LINE__, "bind()");
	}

	/* make the socket a listening one (passive socket)
	 * this makes the socket suitable for accepting any connection request
	 * 10 clients will be placed in the pending connections queue. If more
	 * clients try to connect, they may receive an error or attempt a
	 * retransmission
	 */
	if (listen(srv_sock, 10) == -1) {
		error_at_line(-3, errno, __FILE__, __LINE__, "listen()");
	}
	printf("Listening on %s:%s\n", argv[1], argv[2]);

	// Initialise epoll
	epoll_fd = epoll_create1(0);
	if (epoll_fd == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__,
				"epoll_create1()");
	}
	// watch this socket for clients (socket available for read)
	struct epoll_event ev;
	ev.data.fd = srv_sock;
	ev.events = EPOLLIN;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, srv_sock, &ev);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__, "epoll_ctl()");
	}

	// watch stdin input
	ev.data.fd = STDIN_FILENO;
	ev.events = EPOLLIN;
	result = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, STDIN_FILENO, &ev);
	if (result == -1) {
		error_at_line(-1, errno, __FILE__, __LINE__, "epoll_ctl()");
	}

	all_intersections.size = 0;
	all_intersections.intersections = NULL;

	print_commands();

	server_loop(srv_sock);

	return 0;
}
