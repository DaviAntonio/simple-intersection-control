/*
 *  Simple Intersection Controller - simulate an intersection on a Raspberry Pi
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Simple Intersection Controller.
 *
 *  Simple Intersection Controller is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MESSAGES_H
#define MESSAGES_H

#include <time.h> // struct timespec

#define STR(x) #x
#define XSTR(x) STR(x)

// messages
#define _MSG_LEN 100
#define MSG_LEN (_MSG_LEN)
#define MSG_LEN_STR XSTR(_MSG_LEN)

#define _MAX_MSG_LEN 99
#define MAX_MSG_LEN (_MAX_MSG_LEN)
#define MAX_MSG_LEN_STR XSTR(_MAX_MSG_LEN)

enum msg_event_type {
	INVALID = 0,
	REGULAR = 1,
	PEDESTRIAN_BUTTON_MAIN,
	PEDESTRIAN_BUTTON_AUX,
	CAR_BUTTON_MAIN_1,
	CAR_BUTTON_MAIN_2,
	CAR_BUTTON_AUX_1,
	CAR_BUTTON_AUX_2,
	CAR_SPEED_MAIN_1,
	CAR_SPEED_MAIN_2,
	CAR_GO_AUX_1,
	CAR_GO_AUX_2,
	EMERGENCY_MAIN,
	EMERGENCY_AUX,
	NIGHT_MODE,
	CAR_RUN_RED_LIGHT_MAIN_1,
	CAR_RUN_RED_LIGHT_MAIN_2,
	CAR_RUN_RED_LIGHT_AUX_1,
	CAR_RUN_RED_LIGHT_AUX_2,
	CAR_OVERSPEED_MAIN_1,
	CAR_OVERSPEED_MAIN_2
};

struct intersection_event {
	enum msg_event_type type;
	int payload;
	struct timespec time;
};

void serialise_msg(struct intersection_event, char *, int *);
struct intersection_event deserialise(char *);

#endif
