/*
 *  Simple Intersection Controller - simulate an intersection on a Raspberry Pi
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Simple Intersection Controller.
 *
 *  Simple Intersection Controller is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GPIOS_H
#define GPIOS_H

int get_gpio_sem_red_1();
int get_gpio_sem_red_2();
int get_gpio_sem_yellow_1();
int get_gpio_sem_yellow_2();
int get_gpio_sem_green_1();
int get_gpio_sem_green_2();
int get_gpio_pedestrian_button_1();
int get_gpio_pedestrian_button_2();
int get_gpio_go_sensor_1();
int get_gpio_go_sensor_2();
int get_gpio_speed_sensor_1_a();
int get_gpio_speed_sensor_1_b();
int get_gpio_speed_sensor_2_a();
int get_gpio_speed_sensor_2_b();

void set_gpio_sem_red_1(int);
void set_gpio_sem_red_2(int);
void set_gpio_sem_yellow_1(int);
void set_gpio_sem_yellow_2(int);
void set_gpio_sem_green_1(int);
void set_gpio_sem_green_2(int);
void set_gpio_pedestrian_button_1(int);
void set_gpio_pedestrian_button_2(int);
void set_gpio_go_sensor_1(int);
void set_gpio_go_sensor_2(int);
void set_gpio_speed_sensor_1_a(int);
void set_gpio_speed_sensor_1_b(int);
void set_gpio_speed_sensor_2_a(int);
void set_gpio_speed_sensor_2_b(int);

#endif
