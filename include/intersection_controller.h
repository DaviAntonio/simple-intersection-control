/*
 *  Simple Intersection Controller - simulate an intersection on a Raspberry Pi
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Simple Intersection Controller.
 *
 *  Simple Intersection Controller is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INTERSECTION_CONTROLLER_H
#define INTERSECTION_CONTROLLER_H

#define MAX_EVENTS (100)

void process_events(int, int);
void validate_gpio(int, int, int);

struct watch_go_sensor_args {
	int fd_to_write;
	int gpio;
};

struct watch_speed_sensor_args {
	int fd_to_write;
	int gpio_a;
	int gpio_b;
};

void *watch_go_sensor_1(void *);
void *watch_go_sensor_2(void *);
void *watch_pedestrian_sensor_main(void *);
void *watch_pedestrian_sensor_aux(void *);
void *watch_speed_sensor(void *);

#endif
