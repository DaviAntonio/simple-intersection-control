/*
 *  Simple Intersection Controller - simulate an intersection on a Raspberry Pi
 *  Copyright (C) 2022 Davi Antônio da Silva Santos <antoniossdavi at gmail.com>
 *  This file is part of Simple Intersection Controller.
 *
 *  Simple Intersection Controller is free software: you can redistribute it
 *  and/or modify it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the License,
 *  or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include <stdbool.h>

#include "messages.h"

struct fsm_signal_t {
	bool pedestrian_button_main;
	bool pedestrian_button_aux;
	bool car_button_main;
	bool car_button_aux;
	bool emergency_main;
	bool emergency_aux;
	bool regular;
	bool night;
	bool st_main_red_aux_green_min;
	bool st_main_green_aux_red_min;
};

void st_main_red_aux_red_0();
void st_main_green_aux_red_min();
void st_main_green_aux_red_max();
void st_main_yellow_aux_red();
void st_main_red_aux_red_1();
void st_main_red_aux_green_min();
void st_main_red_aux_green_max();
void st_main_red_aux_yellow();

void st_main_yellow_aux_yellow();
void st_main_off_aux_off();
void st_main_green_aux_red_emergency();
void st_main_red_aux_green_emergency();

void set_fsm_signal(char);
void set_fsm_signal_1(struct intersection_event);

void set_fsm_state(void (*)());
void next_state();
int get_wait_ms();
void set_wait_ms(int);

bool is_main_red();
bool is_aux_red();

#endif
