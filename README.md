# Simple Intersection Controller

## Compiling

This project depends on the WiringPi library

You only need to run `make clean` and `make`

## Running

Start the `server` with an IPv4 and port.

Start the `double_intersection_controller` with the server's IPv4 and port.
